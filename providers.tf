terraform {
  backend "s3" {
    bucket = "ilan-tf-bucket"
    key    = "terraform.tfstate"
    region = "us-east-1"
  }
}

provider "aws" {
    region = "us-east-1"
}
